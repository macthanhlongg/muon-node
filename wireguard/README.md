<!-- # muon-node -->       

# UPDATE & INSTALL DOCKER 

       sudo apt update && sudo apt upgrade -y 
       sudo apt install docker.io -y
and

       sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
       sudo chmod +x /usr/local/bin/docker-compose

       


## VPN server

       mkdir ~/.wg-easy
       cd ~/.wg-easy
       wget https://www.dropbox.com/s/leqvmh3n4jdefxx/docker-compose.yml
       nano docker-compose.yml
           
  Edit wg_host = extenal ip 
  
   Then change passward

## NAT VPS 


        docker exec wg-easy iptables -t nat -A PREROUTING -p tcp --dport 8011 -j DNAT --to-destination 10.8.0.2

        docker exec wg-easy iptables -t nat -A PREROUTING -p tcp --dport 9011 -j DNAT --to-destination 10.8.0.2

## UNNAT VPS 


        docker exec wg-easy iptables -t nat -D PREROUTING -p tcp --dport 8011 -j DNAT --to-destination 10.8.0.2

    

 IF YOU ARE RUNNING ON GCM DO THIS
      
        docker exec wg-easy iptables -I INPUT -p icmp --icmp-type echo-request -j ACCEPT 
         
### Access server
1/ cat file conf

2/ create client `wg0` > download setting file  

3/ edit wg0.conf remove `::/0`     

4/ save file into client folder ex: `root/vpn/1`

## Client

       cd wireguard
       docker build . -t muon --pull
### Run
1/ replace `root/vpn/1` with place your file `wg0.conf`

       docker run -d \
       --name=muon-0 \
       --cap-add=NET_ADMIN \
       --cap-add=SYS_MODULE \
       -e PUID=1000 \
       -e PGID=1000 \
       --dns 8.8.8.8 \
       -v /root/vpn/1:/config \
       -v /lib/modules:/lib/modules \
       --sysctl net.ipv4.conf.all.src_valid_mark=1 \
       --restart=unless-stopped \
       muon
        
### Config IP 

       sudo docker exec muon-0 ip a

`3: wg0: <POINTOPOINT,NOARP,UP,LOWER_UP> mtu 1420 qdisc noqueue state UNKNOWN group default qlen 1000
link/none 
inet 10.8.0.2/24 scope global wg0
valid_lft forever preferred_lft forever`

1/ ip is `10.8.0.2`

### Check 
        <vps ip>:8000/status
        https://alice.muon.net/join/

### Fix 
* `addedToNetwork":false` > docker restart muon-0

### update 
       docker pull muonnode/muon-node-js
- Start again at settup
