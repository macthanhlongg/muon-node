#!/bin/bash
sleep 40
sudo docker exec wg-easy iptables -t nat -A PREROUTING -p tcp --dport 8011 -j DNAT --to-destination 10.8.0.2
sleep 83s
sudo docker exec wg-easy iptables -t nat -D PREROUTING -p tcp --dport 8011 -j DNAT --to-destination 10.8.0.2
